use adw::prelude::*;

use crate::app;

pub struct AboutDialog {}

impl AboutDialog {
    pub fn show() {
        let window = adw::AboutDialog::builder()
            .application_icon(crate::APP_ID)
            .application_name("Key Rack")
            .version(env!("CARGO_PKG_VERSION"))
            .developer_name("Sophie Herold")
            .developers(vec![
                String::from("Sophie Herold"),
                String::from("Christopher Davis"),
                String::from("Felix Häcker"),
            ])
            .copyright("© 2024 Sophie Herold")
            .license_type(gtk::License::Gpl30)
            .issue_url("https://gitlab.gnome.org/sophie-h/key-rack/-/issues")
            .website("https://gitlab.gnome.org/sophie-h/key-rack")
            // TODO: translation missing
            .translator_credits("translator-credits")
            .build();

        window.present(&app().window())
    }
}
