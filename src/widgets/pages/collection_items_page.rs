use std::cell::RefCell;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{clone, closure, Properties};
use gtk::{glib, CompositeTemplate};

use crate::data::{KrCollection, KrCollectionKind};
use crate::utils::error::DisplayError;
use crate::widgets::KrItemRow;
use crate::{app, data};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[properties(wrapper_type = super::KrCollectionItemsPage)]
    #[template(file = "collection_items_page.ui")]
    pub struct KrCollectionItemsPage {
        #[property(get, set=Self::set_collection)]
        collection: RefCell<Option<KrCollection>>,
        #[template_child]
        unlock_banner: TemplateChild<adw::Banner>,
        #[template_child]
        stack: TemplateChild<gtk::Stack>,
        #[template_child]
        status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        preferences_page: TemplateChild<adw::PreferencesPage>,
        #[template_child]
        preferences_group: TemplateChild<adw::PreferencesGroup>,
        #[template_child]
        item_list: TemplateChild<gtk::ListBox>,
        model: gtk::SortListModel,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for KrCollectionItemsPage {
        const NAME: &'static str = "KrCollectionItemsPage";
        type Type = super::KrCollectionItemsPage;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action_async("collection.lock", None, |obj, _, _| async move {
                if let Some(collection) = obj.collection() {
                    collection
                        .lock()
                        .await
                        .handle_error("Unable to lock collection");
                }
            });

            klass.install_action_async("collection.unlock", None, |obj, _, _| async move {
                if let Some(collection) = obj.collection() {
                    collection
                        .unlock()
                        .await
                        .handle_error("Unable to unlock keyring");
                }
            });

            klass.install_action_async("collection.delete", None, |obj, _, _| async move {
                if let Some(collection) = obj.collection() {
                    let dialog = adw::AlertDialog::new(
                        Some("Delete Keyring?"),
                        Some("All keys in the keyring will be permanently deleted"),
                    );
                    dialog.add_response("cancel", "Cancel");
                    dialog.set_default_response(Some("cancel"));
                    dialog.add_response("delete", "Delete");
                    dialog.set_response_appearance("delete", adw::ResponseAppearance::Destructive);
                    let ret = dialog.choose_future(&obj).await;

                    if ret == "delete" {
                        let res = collection.delete().await;
                        res.handle_error("Unable to delete collection");
                        if res.is_ok() {
                            app().window().show_overview_page();
                        }
                    }
                }
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for KrCollectionItemsPage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            let sorter = gtk::StringSorter::new(Some(&gtk::PropertyExpression::new(
                data::KrItem::static_type(),
                gtk::Expression::NONE,
                "label",
            )));
            self.model.set_sorter(Some(&sorter));

            self.model
                .connect_items_changed(clone!(@weak self as this => move |m, _, _, _|{
                    if m.n_items() == 0 {
                        this.stack.set_visible_child(&*this.status_page);
                    } else {
                        this.stack.set_visible_child(&*this.preferences_page);
                    }
                }));

            self.item_list.bind_model(Some(&self.model), |x| {
                let collection_item: data::KrItem = x.clone().downcast().unwrap();
                KrItemRow::new(collection_item).upcast()
            });

            obj.property_expression("collection")
                .chain_property::<KrCollection>("title")
                .bind(&*self.obj(), "title", gtk::Widget::NONE);

            obj.property_expression("collection")
                .chain_property::<KrCollection>("kind")
                .chain_closure::<String>(closure!(|_: Option<KrCollection>, kind: KrCollectionKind| {
                    match kind {
                        KrCollectionKind::Login => "This keyring is bound to your user and gets automatically unlocked at login.".to_string(),
                        KrCollectionKind::Session => "All keys in this temporary keyring are removed again on logout.".to_string(),
                        _ => "".to_string()
                    }
                }))
                .bind(&*self.preferences_group, "description", gtk::Widget::NONE);

            obj.property_expression("collection")
                .chain_property::<KrCollection>("is-locked")
                .bind(&*self.unlock_banner, "revealed", gtk::Widget::NONE);

            obj.property_expression("collection")
                .chain_property::<KrCollection>("is-locked")
                .chain_closure::<bool>(closure!(|_: Option<KrCollection>, is_locked: bool| {
                    !is_locked
                }))
                .bind(&*self.item_list, "sensitive", gtk::Widget::NONE);

            obj.property_expression("collection")
                .chain_property::<KrCollection>("is-locked")
                .watch(
                    Some(&*obj),
                    clone!(@weak obj => move || {
                        if let Some(collection) = obj.collection() {
                            let is_locked = collection.is_locked();
                            obj.action_set_enabled("collection.lock", !is_locked && !collection.kind().is_system());
                            obj.action_set_enabled("collection.unlock", is_locked);
                        }
                    }),
                );
        }
    }

    impl WidgetImpl for KrCollectionItemsPage {}

    impl NavigationPageImpl for KrCollectionItemsPage {}

    impl KrCollectionItemsPage {
        fn set_collection(&self, collection: KrCollection) {
            let obj = self.obj();

            self.model.set_model(Some(&collection.regular_items()));
            obj.action_set_enabled("collection.delete", !collection.kind().is_system());
            self.collection.borrow_mut().replace(collection);
        }
    }
}

glib::wrapper! {
    pub struct KrCollectionItemsPage(ObjectSubclass<imp::KrCollectionItemsPage>)
        @extends gtk::Widget, adw::NavigationPage;
}

impl KrCollectionItemsPage {
    pub fn show(&self, collection: &KrCollection) {
        app().window().show_collection_items_page();
        self.set_collection(collection);
    }
}
