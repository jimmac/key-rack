use std::cell::RefCell;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;
use gtk::glib::Properties;
use gtk::{glib, CompositeTemplate};

use crate::data::KrFlatpak;
use crate::utils::error::DisplayError;
use crate::widgets::KrItemRow;
use crate::{app, data};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[properties(wrapper_type = super::KrFlatpakItemsPage)]
    #[template(file = "flatpak_items_page.ui")]
    pub struct KrFlatpakItemsPage {
        #[property(get, set=Self::set_flatpak, nullable)]
        flatpak: RefCell<Option<KrFlatpak>>,
        #[template_child]
        app_icon: TemplateChild<gtk::Image>,
        #[template_child]
        app_name: TemplateChild<gtk::Label>,
        #[template_child]
        app_description: TemplateChild<gtk::Label>,
        #[template_child]
        item_list: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub content_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub loading_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        status_page: TemplateChild<adw::StatusPage>,
        #[template_child]
        preferences_page: TemplateChild<adw::PreferencesPage>,
        model: gtk::SortListModel,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for KrFlatpakItemsPage {
        const NAME: &'static str = "KrFlatpakItemsPage";
        type Type = super::KrFlatpakItemsPage;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for KrFlatpakItemsPage {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            let sorter = gtk::StringSorter::new(Some(&gtk::PropertyExpression::new(
                data::KrItem::static_type(),
                gtk::Expression::NONE,
                "label",
            )));
            self.model.set_sorter(Some(&sorter));

            self.item_list.bind_model(Some(&self.model), |x| {
                let collection_item: data::KrItem = x.clone().downcast().unwrap();
                KrItemRow::new(collection_item).upcast()
            });

            self.model
                .connect_items_changed(clone!(@weak self as this => move |m, _, _, _|{
                    if m.n_items() == 0 {
                        this.content_stack.set_visible_child(&*this.status_page);
                    } else {
                        this.content_stack.set_visible_child(&*this.preferences_page);
                    }
                }));

            obj.property_expression("flatpak")
                .chain_property::<KrFlatpak>("icon-name")
                .bind(&*self.app_icon, "icon-name", gtk::Widget::NONE);

            obj.property_expression("flatpak")
                .chain_property::<KrFlatpak>("title")
                .bind(&*self.app_name, "label", gtk::Widget::NONE);

            obj.property_expression("flatpak")
                .chain_property::<KrFlatpak>("description")
                .bind(&*self.app_description, "label", gtk::Widget::NONE);
        }
    }

    impl WidgetImpl for KrFlatpakItemsPage {}

    impl NavigationPageImpl for KrFlatpakItemsPage {}

    #[gtk::template_callbacks]
    impl KrFlatpakItemsPage {
        pub fn set_flatpak(&self, flatpak: Option<&KrFlatpak>) {
            *self.flatpak.borrow_mut() = flatpak.cloned();
            self.model.set_model(flatpak);
        }

        #[template_callback]
        fn string_is_not_empty(&self, label: &str) -> bool {
            !label.is_empty()
        }
    }
}

glib::wrapper! {
    pub struct KrFlatpakItemsPage(ObjectSubclass<imp::KrFlatpakItemsPage>)
        @extends gtk::Widget, adw::NavigationPage;
}

impl KrFlatpakItemsPage {
    pub async fn show(&self, flatpak: &KrFlatpak) -> Result<(), oo7::Error> {
        let imp = self.imp();

        app().window().show_flatpak_items_page();
        imp.loading_stack.set_visible_child(&*self.imp().spinner);

        self.set_flatpak(Some(flatpak.clone()));
        flatpak
            .load_items()
            .await
            .handle_error("Unable to load keys");

        imp.loading_stack.set_visible_child(&*imp.content_stack);

        Ok(())
    }
}
