use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::clone;
use gtk::{glib, CompositeTemplate};

use crate::data::{KrCollection, KrCollectionKind};
use crate::widgets::{KrCollectionRow, KrCreateCollectionDialog};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "collection_list.ui")]
    pub struct KrCollectionList {
        #[template_child]
        pub list: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub session_collection_listbox: TemplateChild<gtk::ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for KrCollectionList {
        const NAME: &'static str = "KrCollectionList";
        type Type = super::KrCollectionList;
        type ParentType = adw::PreferencesGroup;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action("collection.show-create-dialog", None, |obj, _, _| {
                KrCreateCollectionDialog::new().present(obj);
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for KrCollectionList {
        fn constructed(&self) {
            self.parent_constructed();

            let secret_service = crate::app().secret_service();

            let sorter = gtk::CustomSorter::new(|a, b| {
                let a = a.downcast_ref::<KrCollection>().unwrap();
                let b = b.downcast_ref::<KrCollection>().unwrap();

                if a.kind() == KrCollectionKind::Login {
                    return gtk::Ordering::Smaller;
                }

                if b.kind() == KrCollectionKind::Login {
                    return gtk::Ordering::Larger;
                }

                a.title().cmp(&b.title()).into()
            });

            let sorted = gtk::SortListModel::new(Some(secret_service.clone()), Some(sorter));

            let filter = gtk::CustomFilter::new(|o| {
                let collection: &KrCollection = o.downcast_ref().unwrap();
                collection.kind() != KrCollectionKind::Session
            });
            let filtered = gtk::FilterListModel::new(Some(sorted), Some(filter.clone()));

            self.list.bind_model(Some(&filtered), move |o| {
                let collection: &KrCollection = o.downcast_ref().unwrap();
                KrCollectionRow::new(collection).upcast()
            });

            secret_service.connect_session_collection_notify(clone!(@weak self as this => move |ss| {
                let session_collection = ss.session_collection().unwrap();
                let row = KrCollectionRow::new(&session_collection);
                this.session_collection_listbox.append(&row);

                this.session_collection_listbox.set_visible(session_collection.n_items() != 0);
                session_collection.connect_items_changed(clone!(@weak this => move |session, _, _, _| {
                    this.session_collection_listbox.set_visible(session.n_items() != 0);
                }));
            }));
        }
    }

    impl WidgetImpl for KrCollectionList {}

    impl PreferencesGroupImpl for KrCollectionList {}
}

glib::wrapper! {
    pub struct KrCollectionList(ObjectSubclass<imp::KrCollectionList>)
        @extends adw::PreferencesGroup, gtk::Widget, gtk::Box;
}
