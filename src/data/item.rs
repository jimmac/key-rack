use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::rc::Rc;

use adw::prelude::*;
use glib::subclass::prelude::*;
use gtk::glib::{self, WeakRef};
use zeroize::Zeroizing;

use super::KrFlatpak;

mod imp {
    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type=super::KrItem)]
    pub struct KrItem {
        #[property(get)]
        #[property(get=Self::title, name="title")]
        #[property(get=Self::subtitle, name="subtitle")]
        #[property(get=Self::icon_name, name="icon-name")]
        #[property(get=Self::app_id, name="app-id", type=Option<String>)]
        pub label: RefCell<String>,
        #[property(get)]
        pub is_locked: Cell<bool>,

        pub item: RefCell<Option<Rc<Item>>>,
        pub secret: RefCell<Zeroizing<Vec<u8>>>,
        pub attributes: RefCell<HashMap<String, String>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for KrItem {
        const NAME: &'static str = "KrItem";
        type Type = super::KrItem;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for KrItem {}

    impl KrItem {
        pub fn title(&self) -> String {
            if self.obj().is_locked() && self.obj().label().is_empty() {
                "Locked element".to_string()
            } else {
                self.obj().label()
            }
        }

        pub fn subtitle(&self) -> String {
            let attr = self.obj().attributes();

            if let Some(user) = attr
                .get("user")
                .or_else(|| attr.get("username"))
                .or_else(|| attr.get("login"))
            {
                user.to_string()
            } else {
                "".to_string()
            }
        }

        pub fn icon_name(&self) -> String {
            let attr = self.obj().attributes();

            let icon_name = if let Some(schema) = attr.get("xdg:schema") {
                match schema.as_str() {
                    "org.freedesktop.NetworkManager.Connection" => "connection-symbolic",
                    "org.gnome.keyring.NetworkPassword" => "connection-symbolic",
                    "org.gnome.RemoteDesktop.RdpCredentials" => "connection-symbolic",
                    "org.gnome.OnlineAccounts" => "connection-symbolic",
                    "org.gnome.Geary" => "email-symbolic",
                    "org.gnome.Evolution.Data.Source" => "email-symbolic",
                    "org.gnome.Boxes" => "boxes-symbolic",
                    "org.epiphany.FormPassword" => "web-symbolic",
                    "org.gnome.GVfs.Luks.Password" => "drive-symbolic",
                    _ => "key-symbolic",
                }
            } else {
                "key-symbolic"
            };

            icon_name.to_string()
        }

        pub fn app_id(&self) -> Option<String> {
            self.obj()
                .attributes()
                .get("app_id")
                .cloned()
                .and_then(|app_id| {
                    if app_id.is_empty() {
                        None
                    } else {
                        Some(app_id)
                    }
                })
        }
    }
}

glib::wrapper! {
    pub struct KrItem(ObjectSubclass<imp::KrItem>);
}

impl KrItem {
    pub async fn new(item: Item) -> Result<Self, oo7::Error> {
        let obj: Self = glib::Object::new();
        obj.update(item).await?;
        Ok(obj)
    }

    pub async fn update(&self, item: Item) -> Result<(), oo7::Error> {
        let is_locked = match &item {
            Item::Collection(item) => item.is_locked().await?,
            Item::Flatpak(_, _) => false,
        };
        self.imp().is_locked.set(is_locked);
        self.notify_is_locked();

        let label = match &item {
            Item::Collection(item) => item.label().await?,
            Item::Flatpak(item, _) => item.label().to_string(),
        };
        self.imp().label.replace(label);
        self.notify_label();
        self.notify_title();

        let attributes = match &item {
            Item::Collection(item) => item
                .attributes()
                .await?
                .iter()
                .map(|(k, v)| (k.clone(), v.clone()))
                .collect(),
            Item::Flatpak(item, _) => item
                .attributes()
                .iter()
                .map(|(k, v)| (k.clone(), v.to_string()))
                .collect(),
        };

        self.imp().attributes.replace(attributes);
        self.notify_subtitle();
        self.notify_icon_name();
        self.notify_app_id();

        if !is_locked {
            let secret = match &item {
                Item::Collection(item) => item.secret().await?,
                Item::Flatpak(item, _) => item.secret(),
            };
            self.imp().secret.replace(secret);
        }

        self.imp().item.replace(Some(Rc::new(item)));
        Ok(())
    }

    pub async fn set_label(&self, label: &str) -> Result<(), oo7::Error> {
        let item = { self.imp().item.borrow().as_ref().unwrap().clone() };

        match &*item {
            Item::Collection(item) => item.set_label(label).await?,
            Item::Flatpak(item, flatpak) => {
                let mut item = item.clone();
                item.set_label(label);
                flatpak.upgrade().unwrap().replace_item(self, &item).await?;
            }
        };

        *self.imp().label.borrow_mut() = label.to_string();
        self.notify_label();
        self.notify_title();

        Ok(())
    }

    pub fn secret(&self) -> Zeroizing<Vec<u8>> {
        self.imp().secret.borrow().clone()
    }

    pub async fn set_secret(
        &self,
        secret: impl AsRef<[u8]>,
        content_type: &str,
    ) -> Result<(), oo7::Error> {
        let item = { self.imp().item.borrow().as_ref().unwrap().clone() };

        match &*item {
            Item::Collection(item) => item.set_secret(secret, content_type).await?,
            Item::Flatpak(item, flatpak) => {
                let mut item = item.clone();
                item.set_secret(secret);
                flatpak.upgrade().unwrap().replace_item(self, &item).await?;
            }
        };

        Ok(())
    }

    pub async fn delete(&self) -> Result<(), oo7::Error> {
        let item = { self.imp().item.borrow().as_ref().unwrap().clone() };

        match &*item {
            Item::Collection(item) => item.delete().await?,
            Item::Flatpak(_, flatpak) => flatpak.upgrade().unwrap().delete_item(self).await?,
        };

        Ok(())
    }

    pub fn attributes(&self) -> HashMap<String, String> {
        self.imp().attributes.borrow().clone()
    }
}

#[derive(Debug)]
pub enum Item {
    Collection(oo7::dbus::Item<'static>),
    Flatpak(oo7::portal::Item, WeakRef<KrFlatpak>),
}

impl From<oo7::dbus::Item<'static>> for Item {
    fn from(value: oo7::dbus::Item<'static>) -> Self {
        Self::Collection(value)
    }
}

impl From<(oo7::portal::Item, KrFlatpak)> for Item {
    fn from(value: (oo7::portal::Item, KrFlatpak)) -> Self {
        Self::Flatpak(value.0, value.1.downgrade())
    }
}
