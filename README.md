# Key Rack

Key Rack allows to view and edit keys, like passwords or tokens, stored by apps. It supports Flatpak secrets as well as system wide secrets.

![Key Rack](https://gitlab.gnome.org/sophie-h/key-rack/uploads/f129bd26906266c9533790dce11f0f9f/key-rack.png)

